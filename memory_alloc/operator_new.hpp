#include <iostream>										
#include <iomanip>										

class B
{
public:
	B(int y) : _y(y)
	{
	}
	~B()
	{
		std::cout << "~B:"<<_y<<" ";
	}
	
private:
	int _y;
};

class A
{
public:
	A(int x ): _x(x), _b(x){}
	~A(){std::cout<<"~A:"<<_x<<" ";}
	int get_x() const {return _x;}
private:
	B _b;
	int _x;
};

int main() {
	A* array = reinterpret_cast<A*>(operator new(10*sizeof(A)));
	for(int i = 0; i<10; ++i)
		new (array + i) A(i);
		
	for(int i = 0; i<10; ++i)
		std::cout << (array+i)->get_x() << " ";
	
	std::cout << std::endl;
	
	for(int i = 0; i<10; ++i)
		(array + i)->~A();
		
	operator delete (array);
	
	return 0; 								
}														
